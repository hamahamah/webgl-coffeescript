# Main
Time to make a new attempt at three.js. This time I'll increase the pain level by doing it using CoffeeScript. And not only that, I'll use literate CoffeeScript.

## The main module
I have used require.js in earlier CS code so I'll keep using it here. For the first tutorial the only JS libraries needed should be jQuery and three.js so I'll include those in the requirements.

	requirejs [
		'jquery'
		'three'
	], (
		$
		three
	) ->

## The Scene
The scene size is given as globals. This might change later but I'll stick to the tutorial for now.

		WIDTH = 400
		HEIGHT = 300

WebGL will render in an element with the ID container

		$container = $('#container')

Now it's time to create the renderer and a scene.

		renderer = new THREE.WebGLRenderer()
		scene = new THREE.Scene()

Start the renderer

		renderer.setSize WIDTH, HEIGHT

Attach the renderer supplied DOM element to the container element.

		$container.append renderer.domElement

### Sphere
It's time to create something to show in the WebGL viewport we have created above. The tutorial thinks a red sphere is a good idea so who am I to disagree on this.
First let's define some geometry for the sphere. A radius and the number of segments and rings must be specified.

		radius = 50
		segments = 16
		rings = 16

Then the sphere needs a material. A simple lambert material is ok here. And yes, we make it red.

		sphereMaterial = new THREE.MeshLambertMaterial {
			color: 0xCC0000
		}

Now we combine the geometry and the material to a mesh that we add to the scene.

		sphere = new THREE.Mesh (new THREE.SphereGeometry radius, segments, rings), sphereMaterial

		scene.add sphere

### Light
To make sure we see some shading, to prove that it's 3D we are working in here, we need a light source. Otherwise Three defaults to a pure ambient shading of the scene and the sphere will only be a flat red circle. A point light with pure white light does the trick here. This is also added to the scene.

		pointLight = new THREE.PointLight 0xffffff
		pointLight.position.x = 10
		pointLight.position.y = 50
		pointLight.position.z = 130

		scene.add pointLight

### Camera
A camera is needed to know what point of view the scene should be rendered from. We decide on some characteristics of the camera. The camera will have a view angle of 45°. This value is also used to calculate the aspect ratio to be used by the camera. Near and far plane will be at 0.1 and 10000 respectively.

		VIEW_ANGLE = 45
		ASPECT = WIDTH / HEIGHT
		NEAR = 0.1
		FAR = 10000

Then we create a camera based on those values and add the camera to the scene. The camera is moved a little away from the center of the scene. Otherwise the scene would be rendered from inside the spere and that would not be very interesting at all.
	
		camera = new THREE.PerspectiveCamera VIEW_ANGLE, ASPECT, NEAR, FAR
		scene.add camera
		camera.position.z = 300

### Render
Finally we make a single render of the scene so we can se the fruit of our work. We pass the scene and what camera to use when rendering the scene.

		renderer.render scene, camera
