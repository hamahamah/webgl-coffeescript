This first version of the tutorial conversion will be as close to the original as possible.

First define the module and create some base variables.

	startTime	= Date.now()
	container	=
	camera		=
	scene		=
	renderer	=
	stats		=
	cube		= undefined

## Initialization

Lets declare the above mentioned init function.

	init = ->

Checking that WebGL is actually supported. If it isn't the user will get notified.

		Detector.addGetWebGLMessage() unless Detector.webgl

Create a camera passing in FOV, aspect ratio, near plane and far plane.

		camera = new THREE.PerspectiveCamera 70, window.innerWidth / window.innerHeight, 1, 1000
		camera.position.y = 150
		camera.position.z = 350

Create a scene.

		scene = new THREE.Scene()

Create a cube to animate and add it to the scene.

		cube = new THREE.Mesh (new THREE.CubeGeometry 200, 200, 200), new THREE.MeshNormalMaterial()
		cube.position.y = 150
		scene.add cube

Set up a renderer.

		renderer = new THREE.WebGLRenderer()
		renderer.setSize window.innerWidth, window.innerHeight

Insert the viewport in the DOM.

		container = document.createElement 'div'
		container.appendChild renderer.domElement
		document.body.appendChild container

Add a stats view and add it to the DOM.

		stats = new Stats()
		stats.domElement.style.position = 'absolute'
		stats.domElement.style.top = '0px'
		container.appendChild stats.domElement

## Animation

Declare the function that animates and reders the scene.

	animate = ->

The actual rendering is handled in it's own function. The looping is done by calling the render function as often as possible.

		render()
		requestAnimationFrame animate

The stats object also needs to be updated.

		stats.update()

## Rendering

	render = ->
While rendering we also update the rotation and the scale of the cube to make things a little interesting.
First rotate the cube.

		cube.rotation.x += 0.02
		cube.rotation.y += 0.0225
		cube.rotation.z += 0.0175

Then scale it based on the time since the start of the application.

		dtime = Date.now() - startTime
		cube.scale.x =
		cube.scale.y =
		cube.scale.z = 1 + 0.3 * Math.sin dtime / 300

Render the scene.

		renderer.render scene, camera

## Start the application

Then call the init function setting the scene up etc.

	init()

Start animating things.

	animate()
